// use nodemon command in gitbash to start node application
// with the use of nodemon our application automatically restarts when there is/are changes in our file


// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (node JS/express JS applications) communicate by exchanging individual messages.
// The messages sent by the client, usually a Web browser, are called requests
// The messages sent by the server as an answer are called responses.
let http = require("http");


let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@mail.com"
	}
];

// Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client
// The http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
// A port is a virtual point where network connections start and end.
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server
http.createServer(function (request, response) {

	// HTTTP Methods:
		/*
			GET, POST, PUT, DELETE
		*/

	// We use 'GET' method to retrieve data
	if(request.url == "/users" && request.method=="GET"){

		// Here we define what content-type we will receive
		response.writeHead(200, {'Content-Type' : 'application/json'});

		response.write(JSON.stringify(directory));
		// ends the response process
		response.end();

	}

	else if(request.url == "/users" && request.method=="POST"){

		// This will act as placeholder for resource/data to be created later on
		let requestBody = ''; 

		//request.on('requestName', function(parameter)){
		//code block
	
		request.on('data', function(data){ //data is in form of hexadecimals

			// this code is where we transform our hexadecimal data to readable text or object
			requestBody += data; 
		});


		request.on('end', function(){

			console.log(requestBody);

			// this transforms our requested data from JSON object to JS object
			requestBody = JSON.parse(requestBody);

			let newUser = {
				// retrieving the field value using dot notation
				"name" : requestBody.name, 
				"email" : requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type' : 'application/json'});
			response.write(JSON.stringify(newUser)); 

			response.end();

			// Process inside this code block
			/*
				1. JSON object [from postman]
				2. We retrieved it in a form of hexadecimal [data paremeter]
				3. We transform it to stringified object/JSON object [requestBody += data ]
				4. To access (using dot natation) the fields and values we convert the json object to js object using 'JSON.parse', 
				5. Our response is required to be in a form of string, the reason we stringify the JS object 
			*/
			
		});
	}

}).listen(4000);

console.log("Server running at localhost:4000");


